import streamlit as st 
from PIL import Image
import base64

# Set up some CSS
CSS = """
    img {
        border-radius: 50%;
        background-size: cover;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
    }
"""

# Add the CSS to the page
st.write(f'<style>{CSS}</style>', unsafe_allow_html=True)


# Add an image with the custom styles applied
image_1 = Image.open('image/dung.jpg')
image_2 = Image.open("image/quan.jpg")
# st.image(image_1, width=250)


   
st.title("Thông tin cá nhân")
col1, col2 = st.columns(2)
with col1:
   st.subheader("Nguyễn Sĩ Dũng")
#    st.image("https://static.streamlit.io/examples/cat.jpg")
   st.image(image_1, width=150)
   st.markdown(
    """
    ###### MSSV: 20146059
    ###### Trường: ĐH Sư Phạm Kỹ Thuật TP. Hồ Chí Minh
"""
)

with col2:
   st.subheader("Nguyễn Toàn Quân")
#    st.image("https://static.streamlit.io/examples/dog.jpg")
   st.image(image_2, width=150)
   st.markdown(
    """
    ###### MSSV: 20146059 
    ###### Trường: ĐH Sư Phạm Kỹ Thuật TP. Hồ Chí Minh
"""
)
   
@st.cache_data
def get_img_as_base64(file):
    with open(file, "rb") as f:
        data = f.read()
    return base64.b64encode(data).decode()

img = get_img_as_base64("image/image.jpg")


page_img_bg = """
    <style>
        [data-testid="stAppViewContainer"] {
            # background: url("https://cdn.wallpapersafari.com/15/56/rZJpjL.png");
            # background-size: cover;
            # background: linear-gradient(to left, #9fd07f, #8dd75e);

            background-color: #85FFBD;
            background-image: linear-gradient(45deg, #85FFBD 0%, #FFFB7D 50%, #ffffff 100%);

            
        }
        
        [data-testid="stHeader"] {
            background-color: rgba(0,0,0,0);
        }
        
      
        
        [data-testid="stToolbar"] {
            right: 2rem;
        }
        
        [data-testid="stSidebar"] > div:first-child {{
        background-image: url("data:image/png;base64,{img}");
        background-position: 50% 45%;
        background-size: 400%;
        }}
        [data-testid="stSidebarNav"] span {{
        color:white;
        }}
        
     
    </style>
"""
st.markdown(page_img_bg, unsafe_allow_html=True)

